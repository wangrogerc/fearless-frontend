import React from 'react';

class ConferenceForm extends React.Component{
    constructor(props){
    super(props)
    this.state = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxPresentations: '',
        maxAttendees: '',
        locations: []
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
    this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event){
        const value = event.target.value;
        this.setState({name:value})
        
    }
    handleStartChange(event){
        const value = event.target.value
        this.setState({starts:value})
    }
    handleEndChange(event){
        const value = event.target.value
        this.setState({ends:value})
    }
    handleDescriptionChange(event){
        const value = event.target.value
        this.setState({description:value})
    }
    handleMaxPresentationsChange(event){
        const value = event.target.value
        this.setState({maxPresentations:value})
    }
    handleMaxAttendeesChange(event){
        const value = event.target.value
        this.setState({maxAttendees:value})
    }
    handleLocationChange(event){
        const value = event.target.value
        this.setState({location:value})
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees
        delete data.maxAttendees;
        delete data.locations;
        delete data.maxPresentations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: ''    
            };
            this.setState(cleared);

        }
        }

    async componentDidMount(){
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
        }
        else{
            console.log("ohno")
        }
    }

    
    render(){
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit = {this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {this.handleNameChange} value = {this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleStartChange} value = {this.state.starts} data-provide="datepicker" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="Begin">Begin</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleEndChange} value = {this.state.ends} data-provide="datepicker" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="Ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleDescriptionChange} value = {this.state.description} placeholder="Description" required type="text" name="description" id="description" className="form-control"/>
                <label htmlFor="Description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxPresentationsChange} value = {this.state.maxPresentations} placeholder="Maximum Presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="Maximum Presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxAttendeesChange} value = {this.state.maxAttendees} placeholder="Maximum Attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="Maximum Attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange = {this.handleLocationChange} value = {this.state.location} required id="location" name ="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return ( <option key = {location.name} value = {location.id}>{location.name}</option>)
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}}

export default ConferenceForm