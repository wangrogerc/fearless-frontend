
import React from 'react';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route path="/" >
              <Route path="/conferences/new" element={<ConferenceForm />} />
              {/* <Route path="attendees/new" element={<AttendConferenceForm />} /> */}
              <Route path="/locations/new" element={<LocationForm />} />
              <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
            </Route>
            
          </Routes>
      </div>
    </BrowserRouter>






    // <React.Fragment>
    // <Nav />
    // <div className="container">
    //   {<ConferenceForm />}
    //   {<LocationForm />}
    //   {<AttendeesList attendees={props.attendees}/> }
    // </div>
    // </React.Fragment>  
  );
}

export default App;