window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById("loading-conference-spinner");
    const attendeeForm = document.getElementById("create-attendee-form");
    const successAlert = document.getElementById('success-message');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      loadingIcon.classList.add('d-none');
      selectTag.classList.remove('d-none');
      window.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        const attendeeUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
            };

            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                attendeeForm.reset();
                successAlert.classList.remove("d-none")
                const attendeeInfo = await response.json();
                console.log(attendeeInfo);
      };
    })

    
  
  }});