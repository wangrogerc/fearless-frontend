function createCard(name, description, pictureUrl, starts, ends, locationname){
    return `


    <div class="card">
      <img src="${pictureUrl}" class="shadow p-3 mb-5 bg-white rounded">
      
      <div class="card-body">
      <h5 class="card-subtitle mb-2 text-muted">${locationname}</h5>
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
    ${starts} to ${ends}
  </div>
    </div>
 
  `;
}

function showError(){
    return `
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below. Error!
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>`;
}
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
          const column = document.querySelector(`#column-1`);
          column.innerHTML += showError();
          
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let i = 1;
        for (let conference of data.conferences) {
          
          
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const enUSFormatter = new Intl.DateTimeFormat('en-US');
            const details = await detailResponse.json();
            const title = conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts);
            const startString = enUSFormatter.format(starts)
            const ends = new Date(details.conference.ends);
            const endString = enUSFormatter.format(ends)
            const locationname = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startString, endString, locationname);
            const column = document.querySelector(`#column-${i}`);
            console.log(`#column-${i}`)
            console.log(i)
            
            column.innerHTML += html;
            
            if (i == 3){
                i = 1;
            }
            i++
          }
        }
  
      }
    } catch (e) {
        console.error(e)
      // Figure out what to do if an error is raised
    }
  
  });


